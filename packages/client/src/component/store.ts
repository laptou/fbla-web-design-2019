import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const enum Theme
{
    Light = "light",
    Dark = "dark",
    Night = "night"
}

export interface StoreData
{
    userTheme: Theme;
    componentTheme: Theme | null;
    navigations: number;
}

export default new Vuex.Store<StoreData>({
    state: {
        userTheme: Theme.Light,
        componentTheme: null,
        navigations: 0
    },
    getters: {
        theme(state) { return state.componentTheme || state.userTheme; },
        hasNavigated(state) { return state.navigations > 1; }
    },
    mutations: {
        navigated(state) { state.navigations++; },
        userTheme(state, theme: Theme) { state.userTheme = theme; },
        componentTheme(state, theme: Theme) { state.componentTheme = theme; }
    }
});