import { Store } from "vuex";
import VueRouter from "vue-router";

import { StoreData } from "./store";


export default function func<T>(store: Store<StoreData>)
{
    const router = new VueRouter({
        routes: [{
            component: () => import("@page/test.vue"),
            path: "/test"
        }, {
            component: require("@page/home.vue").default,
            path: "/"
        }, {
            component: () => import(/* webpackChunkName: "info" */ "@page/mission.vue"),
            path: "/mission"
        }, {
            component: () => import("@page/pricing.vue"),
            path: "/pricing"
        }, {
            component: () => import(/* webpackChunkName: "info" */ "@page/faq.vue"),
            path: "/faq"
        }, {
            component: () => import("@page/contact.vue"),
            path: "/contact"
        },{
            component: () => import("@page/login.vue"),
            path: "/login"
        }, {
            component: require("@page/404.vue").default,
            path: "*"
        }],
        mode: process.env.NODE_ENV === "development" ? "hash" : "history"
    });

    router.afterEach(() => store.commit("navigated"));

    return router;
};