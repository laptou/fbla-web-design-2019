import Vue from "vue";
import * as v from "av-ts";
import { Theme } from "../store";

@v.Trait export default class Themed extends Vue
{
    public get theme(): Theme
    {
        return this.$store.getters["theme"];
    }
}