import Vue from "vue";
import VueRouter from "vue-router";
import VueOverdrive from "vue-overdrive";
import VueBar from "vuebar";
import VueCalendar from "v-calendar";
import "v-calendar/lib/v-calendar.min.css";

import router from "@component/routes";
import store from "@component/store";

import App from "@component/app.vue";
import NormalLayout from "@layout/normal.vue";
import Button from "@control/button.vue";
import Textfield from "@control/textfield.vue";

import * as Sentry from "@sentry/browser";

Sentry.init({
    dsn: "https://9ce0b32fd41049339669257ec7f87fcb@sentry.io/1334687",
    integrations: [new Sentry.Integrations.Vue({ Vue })],
    environment: process.env.NODE_ENV,
    release: process.env.SENTRY_RELEASE
});

Vue.component("normal-layout", NormalLayout);
Vue.component("ac-button", Button);
Vue.component("ac-textfield", Textfield);

Vue.use(VueRouter);
Vue.use(VueOverdrive);
Vue.use(VueBar);

Vue.use(VueCalendar);

new Vue({
    el: "#app",
    router: router(store),
    store,
    components: { App },
    template: "<App />"
});

if ("serviceWorker" in navigator)
{
    window.addEventListener("load", () =>
    {
        navigator.serviceWorker.register("/service-worker.js")
            .then(r => console.log("SW registered: ", r))
            .catch(r => console.log("SW registration failed: ", r));
    });
}