
declare module "vue-overdrive" {
    import { PluginObject } from "vue";
    const plugin: PluginObject<{}>;
    export default plugin;
}

declare module "vuebar" {
    import { PluginObject } from "vue";
    const plugin: PluginObject<{}>;
    export default plugin;
}

declare module "v-calendar" {
    import { PluginObject } from "vue";
    const plugin: PluginObject<{}>;
    export default plugin;
}

declare module "*.vue" {
    import Vue from "vue";
    export default Vue;
}

declare module "*.scss" {
    const o: { [key: string]: string; };
    export default o;
}