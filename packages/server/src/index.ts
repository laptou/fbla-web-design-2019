import * as fs from "fs";
import * as path from "path";
import * as winston from "winston";
import { format } from "logform";

import * as Koa from "koa";
import * as KoaRouter from "koa-router";
import * as KoaStatic from "koa-static";
import * as KoaSend from "koa-send";
import * as Sentry from "@sentry/node";

const release =
    fs.existsSync(path.join(__dirname, "release")) ?
        fs.readFileSync(path.join(__dirname, "release"), { encoding: "utf8" }) :
        undefined;

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            filename: path.join(__dirname, "../server.log"),
            handleExceptions: true,
            format: format.combine(
                format.timestamp(),
                format.uncolorize(),
                format.metadata(),
                format.simple(),
                format((info, opts) => ({ ...opts, ...info }))({ release })),
            level: "info"
        })
    ]
})

logger.info(`release ${release}`);

Sentry.init({
    dsn: "https://361d211fc959432084d4e050b4431442@sentry.io/1335174",
    release
});

const app = new Koa();
const router = new KoaRouter();
const clientDir = path.join(__dirname, "../../client/dist");

app.use(async (ctx, next) =>
{
    await next();

    if (ctx.status === 404)
        return KoaSend(ctx, "index.html", { root: clientDir });
});

app.use(KoaStatic(clientDir, { gzip: true }));

app.on("error", err =>
{
    Sentry.captureException(err);
});

app.listen(process.env.PORT || 3000);

logger.info("Server started");
Sentry.captureMessage("Server started", Sentry.Severity.Info);